<?php

	function lista($nomeTabela){
		include("conecta.php");
		
		$sql = "SELECT * from $nomeTabela";		
		
		$res = mysqli_query($con, $sql)
			or die("Erro ao listar:<BR>" . mysqli_error($con));
			
		return $res;
	}

	function listaPorCampo($nomeTabela,$campo,$valor){
		include("conecta.php");
		
		$sql = "SELECT * from $nomeTabela WHERE $campo = $valor;";		
		
		$res = mysqli_query($con, $sql)
			or die("Erro ao listar:<BR>" . mysqli_error($con));
			
		return $res;
	}
	
	function listaRefeicao(){
		include("conecta.php");

		$sql = "SELECT id_refeicao_aluno, cardapio.descricao, alunos.nome FROM refeicao_aluno, cardapio, alunos 
				WHERE (cardapio.id_cardapio = refeicao_aluno.id_cardapio) AND (alunos.id_aluno = refeicao_aluno.id_aluno)
				ORDER BY id_refeicao_aluno;";
		
		$res = mysqli_query($con, $sql)
			or die("Erro ao listar:<BR>" . mysqli_error($con));
		
		return $res;
	}

	function listaCardapioOpcoes(){
		include("conecta.php");	
		
		$sql = "SELECT id_cardapio_opcoes, cardapio.descricao as cdesc, opcoes.descricao as odesc FROM cardapio_opcoes, cardapio, opcoes 
				WHERE (cardapio.id_cardapio = cardapio_opcoes.id_cardapio) AND (opcoes.id_opcao = cardapio_opcoes.id_opcao)
				ORDER BY id_cardapio_opcoes;";
		
		$res = mysqli_query($con, $sql)
			or die("Erro ao listar:<BR>" . mysqli_error($con));
		
		return $res;
	}
?>