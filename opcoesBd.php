<html>  
<head>
	<?php $active = 'opcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = ''; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
          $titulo = 'Opções';
          include_once('head.php');
    ?>
	<meta http-equiv="Content-Type">
	<title>Cadastro de Clientes</title>
</head>

<body>
	<?php 
        include_once('navbar.php'); 
	?>
	<div class="container mt-5">
	<div class="row align-items-center">
	<?php
		include_once("conecta.php"); //inclui o codigo de conexão com o banco
        $nome = trim($_POST['nome']); //pega o que foi envinado no campo 'nome'
        $valor_nutritivo = trim($_POST['valor_nutritivo']);
		$validacao = true; //variavel para validar se algum campo não foi preenchido			
		if($nome == null) {
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert">Campo Nome não informado!</div>';
			$validacao = false;
        }
        if($valor_nutritivo == null) {
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert">Campo Valor Nutricional não informado!</div>';
			$validacao = false;
		}
		if($validacao == true){
			echo '<div class="jumbotron bg-light mb-3 col-8 mx-auto border border-dark text-left pl-5 pr-5 pt-4 pb-4">';
			echo '<div class="alert alert-success col-12 text-center" role="alert">Os dados foram preenchidos corretamente!</div>';
			echo '<h1 class="text-center">Os dados informados foram:</h1>';
			
			$sql = "INSERT INTO Opcoes (descricao,valor_nutritivo)
				VALUES ('".$nome."','".$valor_nutritivo."');";
			
			$res = mysqli_query($con, $sql)  
			or die('<div class="alert alert-danger col-12 mx-auto text-center" role="alert"> A inserção no banco falhou. Clique <a href="cardapioCad.php" class="alert-link">aqui</a> para tentar novamente.<br>Erro: ' . mysqli_error($con));
			echo '</div>';

			$id = mysqli_insert_id($con);

			header("Location: opcoesList.php?insert=true&nome=".$nome."&valor_nutritivo=".$valor_nutritivo);
		}else{
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert"> O cadastro falhou. Clique <a href="cardapioCad.php" class="alert-link">aqui</a> para tentar novamente.</div>';
		}	
	?>
	<!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>