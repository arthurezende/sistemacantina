<title>Sistema de Controle da Cantina | <?php echo $titulo; ?></title>
    <meta charset="UTF-8">
    <meta name="author" content="Arthur Rezende e Mikaela Almeida">
    <meta name="description" content="Sistema de Controle da Cantina, Trabalho da Disciplina de Programação Web 2 dos alunos Arthur Rezende e Mikaela Almeida, do IFSULDEMINAS Campus Poços de Caldas.">
    <meta name="keywords" content="Cantina,Sistema,Programação Web, PHP, Sistema de Controle">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilocantina.css" rel="stylesheet">