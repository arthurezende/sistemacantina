<head>
    <?php $active = 'cardapioopcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = 'listar'; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
          $titulo = 'Opções do Cardápio';
          include_once('head.php');
    ?>
</head>
<body>
    <!-- aqui vai a barra de navegação-->
    <?php 
        include_once('navbar.php'); 
        include_once("listagens.php");
        $res = listaCardapioOpcoes();
		
		$count = mysqli_num_rows($res);
	?>
    <div class="container mt-5">
    <?php
    if(isset($_GET["insert"])){
        echo '<div class="alert alert-success" role="alert">';
            echo "Opções inseridas com sucesso no cardápio: '". $_GET["cardapio"]."'.";
        echo "</div>";
    }
		echo "<h1 class='alert-heading'>Listagem de Opções dos Cardápios</h1><BR>";
		
		echo "<h5 class='alert-heading'>Total: $count</h5><BR>";
		
		echo "<table class='table'>";
		echo "<th>ID</th><th>Cardápio</th><th>Opções</th>";
		
		while ($registro = mysqli_fetch_assoc($res)){
            
            $id = $registro['id_cardapio_opcoes'];
            $idaluno = $registro['cdesc'];
            $idcardapio = $registro['odesc'];	

			echo "<tr>";		
            echo "<td>".$id."</td>";
            echo "<td>".$idaluno."</td>";
            echo "<td>".$idcardapio."</td>";
			echo "</tr>";
		}
		
		echo "</table>";
    ?>
    </div>
    <!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>