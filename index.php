<head>
    <?php $active = 'inicio'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = ''; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
          $titulo = 'Início';
          include_once('head.php');
    ?>
</head>
<body>
    <!-- aqui vai a barra de navegação-->
    <?php include_once('navbar.php'); ?>

    <!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>