<head>
    <?php $active = 'cardapioopcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
    $sub = 'cadastrar'; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
    $titulo = 'Opções do Cardápio';
    include_once('head.php');
    ?>
</head>
<body>
    <?php $active = 'cardapioopcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = 'cadastro'; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */?>

    <!-- aqui vai a barra de navegação-->
    <?php include_once('navbar.php'); /* codigo que inclui todas as linhas do arquivo navbar.php*/ 
          include_once("listagens.php");
    ?>

    <div class="container my-5 w-75"> 
      <div class="jumbotron">
        <h1 class="display-3 text-center">Cadastro de Opções do Cardápio</h1>
        <p class="lead text-center">Página do Sistema da Cantina para cadastrar opções dos cardápios.</p>
        <hr class="my-4">
        <form name="cadcliente" action="cardapioopcoesBd.php" method="POST" enctype="application/x-www-form-urlencoded">
        <div class="form-group w-75">
          <label for="listEstados" class="col-sm-2 col-form-label">Cardápio</label>
          <div class="col-sm-10">
          <select class="form-control" name="cardapio">
            <option value="">Selecione</option>
            <?php
              $res = lista("Cardapio");
              
              while ($registro = mysqli_fetch_assoc($res)){
                $id_cardapio = $registro['id_cardapio'];
                $descricao = $registro['descricao'];

                echo "<option value='$id_cardapio'>$descricao</option>";
              }
            ?>
          </select>			
          </div>
        </div>
        <div class="form-group w-75">
          <label for="listEstados" class="col-sm-2 col-form-label">Opções</label>
          <div class="col-sm-10">
          <select class="form-control" name="opcoes[]" multiple>
            <?php
              $res = lista("Opcoes");
              
              while ($registro = mysqli_fetch_assoc($res)){
                $id_opcao = $registro['id_opcao'];
                $descricao = $registro['descricao'];
                
                echo "<option value='$id_opcao'>$descricao</option>";
              }
            ?>
          </select>			
          </div>
        </div>
          <div class="form-group">
            <input class="btn btn-danger btn-sm ml-3" type="submit" value="Cadastrar">
            <input class="btn btn-warning btn-sm" name="btnLimpar" type="reset" value="Limpar"/>
          </div>	
        </form>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>