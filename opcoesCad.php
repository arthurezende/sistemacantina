<head>
    <?php $active = 'opcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
    $sub = 'cadastro'; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
    $titulo = 'Opções';
    include_once('head.php');
    ?>
</head>
<body>
    <!-- aqui vai a barra de navegação-->
    <?php include_once('navbar.php'); ?>

    <div class="container-fluid my-5 w-75"> 
      <div class="jumbotron">
        <h1 class="display-3 text-center">Cadastro de Opções</h1>
        <p class="lead text-center">Página do Sistema da Cantina para cadastrar opções.</p>
        <hr class="my-4">
        <form name="cadcliente" action="opcoesBd.php" method="POST" enctype="application/x-www-form-urlencoded">
          
          <div class="form-group">
            <label for="txtName" class="col-sm-2 col-form-label">Nome</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" value="" name="nome" placeholder="Nome da Opção" maxlength="50">
            </div>
          </div>
          <div class="form-group">
            <label for="txtName" class="col-sm-5 col-form-label">Valor nutritivo</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" style="width:200;" name="valor_nutritivo" onkeypress='return event.charCode >= 48 && event.charCode <= 57' placeholder="Valor nutritivo"></input>
            </div>
          </div>
          <div class="form-group">
            <input class="btn btn-danger btn-sm ml-3" type="submit" value="Cadastrar">
            <input class="btn btn-warning btn-sm" name="btnLimpar" type="reset" value="Limpar"/>
          </div>	
        </form>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>