<nav class="navbar navbar-expand-lg navbar-dark bg-danger">
  <a class="navbar-brand" href="index.php">Sistema de Controle da Cantina</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item <?php if($active == 'inicio'){ echo 'active';} ?>">
        <a class="nav-link" href="index.php">Início</a>
      </li>
      <li class="nav-item dropdown <?php if($active == 'cardapio'){ echo 'active';} ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cardápio
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item <?php if($sub == 'cadastro' && $active == 'cardapio'){ echo 'active';} ?>" href="cardapioCad.php">Cadastrar</a>
          <a class="dropdown-item <?php if($sub == 'listar' && $active == 'cardapio'){ echo 'active';} ?> " href="cardapioList.php">Listar</a>
        </div>
      </li>
      <li class="nav-item dropdown <?php if($active == 'cardapioopcoes'){ echo 'active';} ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Opções do Cardápio
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item <?php if($sub == 'cadastro' && $active == 'cardapioopcoes'){ echo 'active';} ?>" href="cardapioopcoesCad.php">Cadastrar</a>
          <a class="dropdown-item <?php if($sub == 'listar' && $active == 'cardapioopcoes'){ echo 'active';} ?> " href="cardapioopcoesList.php">Listar</a>
        </div>
      </li>
      <li class="nav-item dropdown <?php if($active == 'opcoes'){ echo 'active';} ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Opções
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item <?php if($sub == 'cadastro' && $active == 'opcoes'){ echo 'active';} ?>" href="opcoesCad.php">Cadastrar</a>
          <a class="dropdown-item <?php if($sub == 'listar' && $active == 'opcoes'){ echo 'active';} ?> " href="opcoesList.php">Listar</a>
        </div>
      </li>
      <li class="nav-item dropdown <?php if($active == 'alunos'){ echo 'active';} ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Alunos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item <?php if($sub == 'cadastro' && $active == 'alunos'){ echo 'active';} ?>" href="alunosCad.php">Cadastrar</a>
          <a class="dropdown-item <?php if($sub == 'listar' && $active == 'alunos'){ echo 'active';} ?> " href="alunosList.php">Listar</a>
        </div>
      </li>
      <li class="nav-item dropdown <?php if($active == 'refeicao'){ echo 'active';} ?>">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Refeição
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item <?php if($sub == 'cadastro' && $active == 'refeicao'){ echo 'active';} ?> " href="refeicaoCad.php">Cadastrar</a>
          <a class="dropdown-item <?php if($sub == 'listar' && $active == 'refeicao'){ echo 'active';} ?> " href="refeicaoList.php">Listar</a>
        </div>
      </li>
    </ul>
  </div>
</nav>