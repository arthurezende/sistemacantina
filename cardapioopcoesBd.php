<html>  
<head>
	<?php $active = 'cardapioopcoes'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = ''; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
          $titulo = 'Opções do Cardápio';
          include_once('head.php');
    ?>
	<meta http-equiv="Content-Type">
	<title>Cadastro de Clientes</title>
</head>

<body>
	<?php 
        include_once('navbar.php'); 
	?>
	<div class="container mt-5">
	<div class="row align-items-center">
	<?php
        include_once("conecta.php"); //inclui o codigo de conexão com o banco
		include_once("listagens.php");
		$idcardapio = $_POST['cardapio'];
		$opcoes = $_POST['opcoes'];
		$validacao = true; //variavel para validar se algum campo não foi preenchido
		$res = listaPorCampo('Cardapio','id_cardapio',$idcardapio);
		$registro = mysqli_fetch_assoc($res);
		$cardapio = $registro['descricao'];
		if($idcardapio == null) {
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert">Campo Cardápio não informado!</div>';
			$validacao = false;
		}
		if($opcoes == null) {
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert">Campo Opções não informado!</div>';
			$validacao = false;
		}
		if($validacao == true){
			echo '<div class="jumbotron bg-light mb-3 col-8 mx-auto border border-dark text-left pl-5 pr-5 pt-4 pb-4">';
			echo '<div class="alert alert-success col-12 text-center" role="alert">Os dados foram preenchidos corretamente!</div>';
			echo '<h1 class="text-center">Os dados informados foram:</h1>';
            //echo "Cardápio: $cardapio<BR>";

            foreach ($opcoes as $opcoes){
                $sql = "INSERT into cardapio_opcoes (id_cardapio, id_opcao) VALUES ('".$idcardapio."','".$opcoes."');";
                $res = mysqli_query($con, $sql)  
                or die('<div class="alert alert-danger col-12 mx-auto text-center" role="alert"> A inserção no banco falhou. Clique <a href="cardapioCad.php" class="alert-link">aqui</a> para tentar novamente.<br>Erro: ' . mysqli_error($con));
                echo '</div>';
            }
			// $sql = "INSERT INTO Cardapio_Opcoes (id_cardapio,id_opcao)
			// 	VALUES ('".$cardapio."','".$opcao."');";
			
			// $res = mysqli_query($con, $sql)  
			// or die('<div class="alert alert-danger col-12 mx-auto text-center" role="alert"> A inserção no banco falhou. Clique <a href="cardapioCad.php" class="alert-link">aqui</a> para tentar novamente.<br>Erro: ' . mysqli_error($con));
			// echo '</div>';

			// $id = mysqli_insert_id($con);
			header("Location: cardapioopcoesList.php?insert=true&cardapio=".$cardapio);
		}else{
			echo '<div class="alert alert-danger col-12 mx-auto text-center" role="alert"> O cadastro falhou. Clique <a href="cardapioCad.php" class="alert-link">aqui</a> para tentar novamente.</div>';
		}	
	?>
	<!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>

</html>