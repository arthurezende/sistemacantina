<head>
    <?php $active = 'refeicao'; /* nome da pagina atual para setar como 'active' na barra de menu */ 
          $sub = 'listar'; /* caso esta seja uma sub pagina (ex. cadastro cardapio, subpagina de cardapio) */
          $titulo = 'Refeição';
          include_once('head.php');
    ?>
</head>
<body>
    <!-- aqui vai a barra de navegação-->
    <?php 
        include_once('navbar.php'); 
        include_once("listagens.php");
        $res = listaRefeicao();
		
		$count = mysqli_num_rows($res);
	?>
    <div class="container mt-5">
    <?php
    if(isset($_GET["insert"])){
        echo '<div class="alert alert-success" role="alert">';
            echo "Registro inserido com sucesso - Aluno: ". $_GET["aluno"]."; Cardápio: ". $_GET["cardapio"];
        echo "</div>";
    }
		echo "<h1 class='alert-heading'>Listagem de Refeições</h1><BR>";
		
		echo "<h5 class='alert-heading'>Total de refeições: $count</h5><BR>";
		
		echo "<table class='table'>";
		echo "<th>ID da Refeição</th><th>Aluno</th><th>Opção</th>";
		
		while ($registro = mysqli_fetch_assoc($res)){
            
            $id = $registro['id_refeicao_aluno'];
            $idaluno = $registro['nome'];
            $idcardapio = $registro['descricao'];	

			echo "<tr>";		
            echo "<td>".$id."</td>";
            echo "<td>".$idaluno."</td>";
            echo "<td>".$idcardapio."</td>";
			echo "</tr>";
		}
		
		echo "</table>";
    ?>
    </div>
    <!-- Bootstrap core JavaScript -->
    <!-- ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="./js/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="./js/jquery.min.js"><\/script>')</script>
        <script src="./js/popper.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="./js/ie10-viewport-bug-workaround.js"></script>
</body>